module "us-east-1" {
  source = "./modules/multi-region"

  providers = {
    aws = aws.use1
  }
}

module "us-west-2" {
  source = "./modules/multi-region"

  providers = {
    aws = aws.usw2
  }
}