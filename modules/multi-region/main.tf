module "transit-gateway" {
	source = "terraform-aws-modules/transit-gateway/aws"

	name = "ddye-terraform-tgw-useast1"
	description = "testing TGW using Terraform"

	enable_auto_accept_shared_attachments = true
	ram_allow_external_principals = true
	ram_principals = [837097314487]
}